package com.example.montagarfa.tunevent.ui.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.Session;
import com.example.montagarfa.tunevent.dataServices.CategoryWebService;
import com.example.montagarfa.tunevent.dataServices.EventWebService;
import com.example.montagarfa.tunevent.model.Category;
import com.example.montagarfa.tunevent.model.Event;
import com.example.montagarfa.tunevent.netowrk.RetrofitClientInstance;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddEventFragment extends Fragment {
    private static final String TAG = "AddEventFragment";
    Session session;
    Event event;
    @BindView(R.id.event_name)
    TextView eventname;
    @BindView(R.id.event_location_1)
    AppCompatSpinner eventlocation;
    @BindView(R.id.event_nbplace)
    TextView eventnbplace;
    @BindView(R.id.event_date_1)
    TextView eventdate;
    @BindView(R.id.event_category_1)
    AppCompatSpinner eventCategory;
    @BindView(R.id.event_photo)
    TextView eventphoto;
    @BindView(R.id.btn_addevent)
    Button btnaddevent;

    ArrayList<String> location = new ArrayList<String>();
    ArrayList<String> categoriesS = new ArrayList<String>();
    ArrayList<Category> categories = new ArrayList<Category>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_addevent, null);
        ButterKnife.bind(this, view);
        initSpinner();
        session = Session.getSesstion(getContext());
        return view;
    }

    public void initSpinner() {
        final ProgressDialog progress = new ProgressDialog(getActivity(),
                R.style.SpinnerTheme);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("Loading ...");
        progress.show();
        initCategory();
        initLocation();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        progress.dismiss();
                    }
                }, 1000);
        btnaddevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEvent();

            }
        });

    }

    public void initLocation() {

        location.add("Tunis");
        location.add("France");
        location.add("USA");
        location.add("Italie");
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getContext(), R.layout.spinner_item, location);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        eventlocation.setAdapter(spinnerArrayAdapter);
    }

    public void initCategory() {

        CategoryWebService categoryData = RetrofitClientInstance.getRetrofitInstance().create(CategoryWebService.class);
        Call<List<Category>> category = categoryData.getAllCategory();
        System.out.println("init app");
        category.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                System.out.println("all is Ok");
                System.out.println(response.code());
                System.out.println(response.body());
                if (response.isSuccessful()) {
                    categories = (ArrayList<Category>) response.body();
                    for (Category category1 : response.body()) {
                        categoriesS.add(category1.getNameCategory());
                    }
                } else {
                    eventCategory.setClickable(false);

                }

            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                eventCategory.setClickable(false);
            }
        });

        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getContext(), R.layout.spinner_item, categoriesS);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        eventCategory.setAdapter(spinnerArrayAdapter);


    }

    public void addEvent() {
        if (!validate()) {
            Toast.makeText(getActivity(), "Check Inputs ...", Toast.LENGTH_LONG).show();

            return;
        }
        final ProgressDialog progress = new ProgressDialog(getContext(),
                R.style.SpinnerTheme);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("Loading ...");
        progress.show();
        event = new Event();
        event.setNameEvent(eventname.getText().toString());
        event.setLocation(location.get(eventlocation.getSelectedItemPosition()));
        //event.setCategory(categories.get(2));
        event.setNbOfDplace(Long.valueOf(eventnbplace.getText().toString()));
        event.setNbOfDplace(Long.valueOf(eventnbplace.getText().toString()));
        event.setOrganizer(session.getUser());
        DateFormat dateFormat = new SimpleDateFormat();
        /*try {
            event.setDate(dateFormat.parse(eventdate.getText().toString()));
        } catch (ParseException e) {
            event.setDate(new Date());
        }*/
        event.setPhoto_uri(eventphoto.getText().toString());


        EventWebService eventWebService = RetrofitClientInstance.getRetrofitInstance().create(EventWebService.class);
        Call<Event> eventCall = eventWebService.addEvent(event, "Hackhaton");
        eventCall.enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                Log.d(TAG, "onResponse: " + response.code());
                if (response.isSuccessful()) {
                    if (response.code() == 201) {
                        System.out.println("");
                        Toast.makeText(getActivity(), "Event added ...", Toast.LENGTH_LONG).show();
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragment_container, new HomeFragment())
                                .commit();

                    } else {
                        Toast.makeText(getActivity(), "Failed to add Event", Toast.LENGTH_LONG).show();

                    }

                }
                progress.dismiss();

            }

            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(getActivity(), "Failed to add Event", Toast.LENGTH_LONG).show();

            }
        });

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        // onLoginSuccess();
                        //monta.garfa@gmail.com onLoginFailed();
                    }
                }, 3000);

    }

    public boolean validate() {
        boolean valid = true;
        String eventN = eventname.getText().toString();
        String eventNB = eventnbplace.getText().toString();
        String eventP = eventphoto.getText().toString();
        String eventD = eventdate.getText().toString();

        if (eventN.isEmpty()) {
            eventname.setError("at least 3 chafragment_containerracters");
            valid = false;
        } else {
            eventname.setError(null);
        }

        if (eventNB.isEmpty()) {
            eventnbplace.setError(" input number of place");
            valid = false;
        } else {
            eventnbplace.setError(null);
        }


        if (eventD.isEmpty()) {
            eventdate.setError("enter a valid date ");
            valid = false;
        } else {
            eventdate.setError(null);
        }

        if (eventP.isEmpty() || !Patterns.WEB_URL.matcher(eventP).matches()) {
            eventphoto.setError("enter a valid url phoyo");
            valid = false;
        } else {
            eventphoto.setError(null);
        }

        return valid;
    }
}
