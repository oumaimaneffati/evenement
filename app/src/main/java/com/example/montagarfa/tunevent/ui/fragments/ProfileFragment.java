package com.example.montagarfa.tunevent.ui.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.Session;
import com.example.montagarfa.tunevent.dataServices.UserWebService;
import com.example.montagarfa.tunevent.model.User;
import com.example.montagarfa.tunevent.netowrk.RetrofitClientInstance;
import com.example.montagarfa.tunevent.ui.activities.SignupActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {
    private static final int REQUEST_SIGNUP = 0;
    private static final String TAG = "ProfileFragment";
    @BindView(R.id.input_email)
    EditText email;
    @BindView(R.id.input_password)
    EditText password;
    @BindView(R.id.btn_login)
    AppCompatButton login;
    @BindView(R.id.link_signup)
    TextView signup;
    private Session session;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: load");
        session = Session.getSesstion(getContext().getApplicationContext());
        View view = inflater.inflate(R.layout.fragment_profile, null);
        ButterKnife.bind(this, view);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: logn");
                login();

            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: signup");

                Intent intent = new Intent(getContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                getActivity().overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        return view;
    }

    // login pressed
    public void login() {
        if (!validate()) {
            onLoginFailed();
            return;
        }

        final ProgressDialog progress = new ProgressDialog(getActivity(),
                R.style.SpinnerTheme);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("Loading ...");
        progress.show();
        String emailV = email.getText().toString();
        String passwordV = password.getText().toString();
        // TODO: 11/12/18
        UserWebService userWebService = RetrofitClientInstance.getRetrofitInstance().create(UserWebService.class);
        Call<User> userCall = userWebService.getUser(emailV, passwordV);
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        session.setUser(response.body());
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragment_container, new AloginFragment())
                                .commit();
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragment_container, new AloginFragment())
                                .commit();

                    } else {
                        Toast.makeText(getActivity(), "Failed to login", Toast.LENGTH_LONG).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getActivity(), "Failed to load data", Toast.LENGTH_LONG).show();

            }
        });

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        // onLoginSuccess();
                        //monta.garfa@gmail.com onLoginFailed();
                        progress.dismiss();
                    }
                }, 1000);
    }

    //validate mail password
    public boolean validate() {
        boolean valid = true;
        String emailV = email.getText().toString();
        String passwordV = password.getText().toString();
        if (emailV.isEmpty()) {
            email.setError("enter a valid email address");
            valid = false;
        }
        if (passwordV.isEmpty() || passwordV.length() < 4) {
            password.setError("enter a valid password");
            valid = false;
        }
        return valid;
    }

    // login failed
    public void onLoginFailed() {
        Toast.makeText(getContext(), "Login failed", Toast.LENGTH_LONG).show();

        login.setEnabled(true);
    }

    //login success
    public void onLoginSuccess() {
        login.setEnabled(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
