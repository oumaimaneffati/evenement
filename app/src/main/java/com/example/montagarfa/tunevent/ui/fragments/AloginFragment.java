package com.example.montagarfa.tunevent.ui.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.Session;
import com.example.montagarfa.tunevent.model.User;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AloginFragment extends Fragment {
    private static final String TAG = "AloginFragment";
    Session session;
    User appuser;
    @BindView(R.id.email_user)
    TextView email;
    @BindView(R.id.firstname_user)
    TextView firstname;
    @BindView(R.id.lastname_user)
    TextView lastname;
    @BindView(R.id.image_user)
    ImageView image;
    @BindView(R.id.btn_logout)
    Button btnLogOut;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alogin, null);
        ButterKnife.bind(this, view);
        session = Session.getSesstion(getContext().getApplicationContext());
        getUser();
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        return view;
    }

    public void logout() {

        // TODO: 11/12/18
        final ProgressDialog progress = new ProgressDialog(getActivity(),
                R.style.SpinnerTheme);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("Loading ...");
        progress.show();
        session.logout();
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new ProfileFragment())
                .commit();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        //monta.garfa@gmail.com onLoginFailed();
                        progress.dismiss();
                    }
                }, 2000);

    }

    private void getUser() {
        appuser = session.getUser();
        email.setText(appuser.getUsername());
        firstname.setText(appuser.getFirstName());
        lastname.setText(appuser.getLastName());
/*               Glide.with(getContext())
                        .asBitmap()
                        .load("https://i.redd.it/j6myfqglup501.jpg")
                        .into(image);*/
    }

}
