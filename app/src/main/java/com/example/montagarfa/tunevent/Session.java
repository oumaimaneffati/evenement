package com.example.montagarfa.tunevent;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.montagarfa.tunevent.model.User;

public class Session {
    private static Session instance = null;
    private SharedPreferences sharedPreferences;
    private User user = new User();
    private boolean loggin;

    private Session(Context cntx) {

        // TODO Auto-generated constructor stub
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public static synchronized Session getSesstion(Context context) {
        if (instance == null)
            instance = new Session(context);
        return instance;
    }

    public User getUser() {
        user.setIdUser(sharedPreferences.getString("idUser", ""));
        user.setFirstName(sharedPreferences.getString("firstName", ""));
        user.setLastName(sharedPreferences.getString("lastName", ""));
        user.setUsername(sharedPreferences.getString("username", ""));
        user.setPassword(sharedPreferences.getString("password", ""));
        user.setPhoto_uri(sharedPreferences.getString("photo_uri", ""));
        return user;
    }

    public void setUser(User user) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("idUser", user.getIdUser());
        edit.putString("firstName", user.getFirstName());
        edit.putString("lastName", user.getLastName());
        edit.putString("username", user.getUsername());
        edit.putString("password", user.getPassword());
        edit.putString("photo_uri", user.getPhoto_uri());
        edit.putBoolean("loggedin", true);
        edit.commit();
    }

    public boolean isLoggin() {
        return sharedPreferences.getBoolean("loggedin", false);
    }

    public void logout() {
        sharedPreferences.edit().clear().commit();
    }
}
