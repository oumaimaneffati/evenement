package com.example.montagarfa.tunevent.ui.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.dataServices.EventWebService;
import com.example.montagarfa.tunevent.model.Event;
import com.example.montagarfa.tunevent.netowrk.RetrofitClientInstance;
import com.example.montagarfa.tunevent.ui.recyclerView.HomeRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {
    private static String TAG = "Home Fragment  ";
    View view;
    RecyclerView recyclerView;

    // list of events
    private ArrayList<Event> listEvent = new ArrayList<Event>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, null);
        initListEvents(view);
        return view;
    }

    private void initListEvents(View view) {

        final ProgressDialog progress = new ProgressDialog(getActivity(),
                R.style.SpinnerTheme);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("Loading ...");
        progress.show();
        EventWebService eventData = RetrofitClientInstance.getRetrofitInstance().create(EventWebService.class);
        Call<List<Event>> eventList = eventData.getAllEvents();
        eventList.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                Log.d(TAG, "onResponse: " + response.code());
                if (response.isSuccessful()) {
                    Log.e(TAG, "" + response.body());
                    listEvent = (ArrayList<Event>) response.body();
                    initRecyledView(view);
                } else {
                    Toast.makeText(getActivity(), "Failed to load data", Toast.LENGTH_LONG).show();

                }
                Log.d(TAG, "" + response.body());

            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Toast.makeText(getActivity(), "Failed to load data", Toast.LENGTH_LONG).show();
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        //monta.garfa@gmail.com onLoginFailed();
                        progress.dismiss();
                    }
                }, 3000);

    }

    private void initRecyledView(View view) {
        Log.d(TAG, "" + listEvent.size());
        Log.d(TAG, "initRecyclerView: init recyclerview.");
//        RecycleViewAdapter adapter = new RecycleViewAdapter(this, mNames, mImageUrls);
        //  SearchRecyclerViewAdapter adapter = new SearchRecyclerViewAdapter(getActivity(),listEvent);
        recyclerView = (RecyclerView) view.findViewById(R.id.recylce_view_home);

        HomeRecyclerViewAdapter adapter1 = new HomeRecyclerViewAdapter(getContext(), listEvent);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter1);

    }


}
