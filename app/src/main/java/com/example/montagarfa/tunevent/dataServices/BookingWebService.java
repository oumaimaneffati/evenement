package com.example.montagarfa.tunevent.dataServices;

import com.example.montagarfa.tunevent.model.Booking;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface BookingWebService {
    @GET("api/booking/all")
    List<Booking> getAllBooking();

    @GET("api/booking/{id}")
    Booking getBookingById(@Path("id") String id);

    @POST("api/booking/")
    Booking addBooking(@Body Booking event);

    @DELETE("api/booking/{id}")
    void deleteBookingById(@Path("id") String id);
}
