package com.example.montagarfa.tunevent.ui.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.dataServices.CategoryWebService;
import com.example.montagarfa.tunevent.model.Category;
import com.example.montagarfa.tunevent.netowrk.RetrofitClientInstance;
import com.example.montagarfa.tunevent.ui.activities.SearchActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment {
    private static final int REQUEST_SIGNUP = 0;
    private static final String TAG = "SearchFragment";
    @BindView(R.id.search_date)
    AppCompatSpinner search_date;
    @BindView(R.id.search_location)
    AppCompatSpinner search_location;
    @BindView(R.id.search_category)
    AppCompatSpinner search_category;
    @BindView(R.id.btn_search)
    Button btnsearch;
    ArrayList<String> arrayDate = new ArrayList<String>();
    ArrayList<String> arrayDate1 = new ArrayList<String>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, null);

        final ProgressDialog progress = new ProgressDialog(getActivity(),
                R.style.SpinnerTheme);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("Loading ...");
        progress.show();
        ButterKnife.bind(this, view);
        initListDate();
        initListLocation();
        initListSearch();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        progress.dismiss();
                    }
                }, 1000);
        btnsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });
        return view;
    }

    public void search() {
        Log.d(TAG, "onClick: signup");
        if (validate()) {
            Intent intent = new Intent(getContext(), SearchActivity.class);
            intent.putExtra("dateSearch", search_date.getSelectedItem().toString());
            intent.putExtra("locationSearch", search_location.getSelectedItem().toString());
            intent.putExtra("categorySearch", search_category.getSelectedItem().toString());
            startActivityForResult(intent, REQUEST_SIGNUP);
            getActivity().overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
    }

    public boolean validate() {
        boolean valid = true;
        String location = search_location.getSelectedItem().toString();
        String date = search_date.getSelectedItem().toString();
        String category = search_category.getSelectedItem().toString();
        if (location.isEmpty()) {
            Toast.makeText(getContext(), "Failed : CHECK LOCATION .", Toast.LENGTH_LONG).show();

            valid = false;
        }
        if (date.isEmpty()) {
            Toast.makeText(getContext(), "Failed : CHECK DATE .", Toast.LENGTH_LONG).show();
            valid = false;

        }
        if (category.isEmpty()) {
            Toast.makeText(getContext(), "Failed : CHECK CATEGORY .", Toast.LENGTH_LONG).show();
            valid = false;

        }

        return valid;
    }

    public void initListDate() {
        arrayDate.add("ALL");
        arrayDate.add("Today");
        arrayDate.add("Tomorrow");
        arrayDate.add("This week");
        arrayDate.add("This mouth");
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getContext(), R.layout.spinner_item, arrayDate);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        search_date.setAdapter(spinnerArrayAdapter);
    }

    public void initListLocation() {
        arrayDate1.add("ALL");
        arrayDate1.add("Tunis");
        arrayDate1.add("France");
        arrayDate1.add("USA");
        arrayDate1.add("Italie");
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getContext(), R.layout.spinner_item, arrayDate1);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        search_location.setAdapter(spinnerArrayAdapter);

    }

    public void initListSearch() {

        arrayDate.add("ALL");
        CategoryWebService categoryData = RetrofitClientInstance.getRetrofitInstance().create(CategoryWebService.class);
        Call<List<Category>> category = categoryData.getAllCategory();
        System.out.println("init app");
        category.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                System.out.println("all is Ok");
                System.out.println(response.code());
                System.out.println(response.body());
                if (response.isSuccessful()) {
                    for (Category category1 : response.body()) {
                        arrayDate.add(category1.getNameCategory());
                    }
                } else {
                    search_category.setClickable(false);

                }

            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                search_category.setClickable(false);
            }
        });

        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getContext(), R.layout.spinner_item, arrayDate);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        search_category.setAdapter(spinnerArrayAdapter);
    }

}

