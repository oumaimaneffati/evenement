package com.example.montagarfa.tunevent;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.example.montagarfa.tunevent.model.Event;
import com.example.montagarfa.tunevent.ui.fragments.AddEventFragment;
import com.example.montagarfa.tunevent.ui.fragments.AloginFragment;
import com.example.montagarfa.tunevent.ui.fragments.HomeFragment;
import com.example.montagarfa.tunevent.ui.fragments.ProfileFragment;
import com.example.montagarfa.tunevent.ui.fragments.SearchFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private static String TAG = " Main   Activity  ";

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;
    private Session session;
    // list of events
    private ArrayList<Event> listEvent = new ArrayList<Event>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        session = Session.getSesstion(getApplicationContext());
        Log.d(TAG, "onCreate :started");
        ButterKnife.bind(this);
        bottomNavigation.setOnNavigationItemSelectedListener(this);
        loadFragment(new HomeFragment());
    }


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.navigtion_home:
                fragment = new HomeFragment();
                break;
            case R.id.navigtion_profile: {
                if (!session.isLoggin())
                    fragment = new ProfileFragment();
                else fragment = new AloginFragment();
            }
            break;
            case R.id.navigtion_search:
                fragment = new SearchFragment();
                break;
            case R.id.navigtion_addevent: {
                if (!session.isLoggin())
                    fragment = new ProfileFragment();
                else fragment = new AddEventFragment();
            }
            break;

        }
        return loadFragment(fragment);
    }
}
