package com.example.montagarfa.tunevent.ui.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.model.Event;

import org.w3c.dom.Text;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class EventActivity extends AppCompatActivity {
    private static final String TAG = "EventActivity ";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_event);
        Log.d(TAG, "onCreate: started.");
        getIncomingIntent();

    }

    private void getIncomingIntent() {
        Log.d(TAG, "getIncomingIntent: checking for incoming intent.");
        if (getIntent().hasExtra("event")) {
            Log.d(TAG, "getIncomingIntent: found intent extra.");
            Event event = (Event) getIntent().getSerializableExtra("event");
            setEvent(event);
        }
    }

    private void setEvent(Event event) {
        ImageView imageView = findViewById(R.id.event_image_detail);
        TextView textView = findViewById(R.id.event_title_detail);
        TextView textViewdate=findViewById(R.id.textViewdate);
        TextView textViewlieu=findViewById(R.id.textViewlieu);
        TextView nbplace=findViewById(R.id.nbplace);

        textView.setText(event.getNameEvent());
        textViewlieu.setText(event.getLocation());
        textViewdate.setText(event.getDate().toString());
        nbplace.setText(event.getNbOfPlace().toString());
        Glide.with(this)
                .asBitmap()
                .load(event.getPhoto_uri())
                .into(imageView);


    }
}
