package com.example.montagarfa.tunevent.ui.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.dataServices.EventWebService;
import com.example.montagarfa.tunevent.model.Event;
import com.example.montagarfa.tunevent.netowrk.RetrofitClientInstance;
import com.example.montagarfa.tunevent.ui.recyclerView.SearchRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {
    private static final String TAG = "SearchActivity";
    ArrayList<Event> events = new ArrayList<Event>();

    private String location;
    private String date;
    private String category;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Log.d(TAG, "onCreate: started.");
        final ProgressDialog progress = new ProgressDialog(this,
                R.style.SpinnerTheme);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("Loading ...");
        progress.show();
        initListEvent();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        progress.dismiss();
                    }
                }, 2000);
    }

    private void initListEvent() {
        Log.d(TAG, "ini list event: preparing .");

        getIntentValues();
        Log.d(TAG, "initListEvent location: " + location);
        Log.d(TAG, "initListEvent date: " + date);
        Log.d(TAG, "initListEvent category: " + category);
        EventWebService eventData = RetrofitClientInstance.getRetrofitInstance().create(EventWebService.class);
        Call<List<Event>> eventList = eventData.getAllEvents(date, location, category);
        eventList.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                Log.d(TAG, "onResponse: " + response.code());
                if (response.isSuccessful()) {
                    Log.e(TAG, "" + response.body());
                    for (Event event : response.body()) {
                        events.add(event);
                        initRecyledView();
                    }
                } else {
                    Toast.makeText(getBaseContext(), "Failed to load data", Toast.LENGTH_LONG).show();

                }
                Log.d(TAG, "" + response.body());

            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Toast.makeText(getBaseContext(), "Failed to load data", Toast.LENGTH_LONG).show();
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });

    }

    private void initRecyledView() {
        System.out.println("garfa montassar");
        Log.d(TAG, "" + events.size());
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = findViewById(R.id.recylce_view_searcj);
        // RecycleViewAdapter adapter = new RecycleViewAdapter(this, mNames, mImageUrls);
        //  SearchRecyclerViewAdapter adapter = new SearchRecyclerViewAdapter(this,listEvent);
        SearchRecyclerViewAdapter adapter1 = new SearchRecyclerViewAdapter(this, events);
        recyclerView.setAdapter(adapter1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    public void getIntentValues() {
        if (getIntent().hasExtra("locationSearch"))
            location = getIntent().getStringExtra("locationSearch");
        else {
            location = "ALL";
        }
        if (getIntent().hasExtra("dateSearch"))
            date = getIntent().getStringExtra("dateSearch");
        else {
            date = "ALL";
        }
        if (getIntent().hasExtra("categorySearch"))
            category = getIntent().getStringExtra("categorySearch");
        else {
            category = "ALL";
        }
        Log.d(TAG, "initListEvent location: " + location);
        Log.d(TAG, "initListEvent date: " + date);
        Log.d(TAG, "initListEvent category: " + category);
    }

}