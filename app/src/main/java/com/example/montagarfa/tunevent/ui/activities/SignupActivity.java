package com.example.montagarfa.tunevent.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.montagarfa.tunevent.MainActivity;
import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.Session;
import com.example.montagarfa.tunevent.dataServices.UserWebService;
import com.example.montagarfa.tunevent.model.User;
import com.example.montagarfa.tunevent.netowrk.RetrofitClientInstance;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";
    Session session;
    @BindView(R.id.input_firstname)
    EditText firstname;
    @BindView(R.id.input_lastname)
    EditText lastname;
    @BindView(R.id.input_email)
    EditText email;
    @BindView(R.id.input_password)
    EditText password;
    @BindView(R.id.input_reEnterPassword)
    EditText repassword;
    @BindView(R.id.input_photo_uri)
    EditText photo_uri;
    @BindView(R.id.btn_signup)
    AppCompatButton signup;
    @BindView(R.id.link_login)
    TextView login;
    private User user;
    private String sharedpreffile = "com.example.montagarfa.tunevent";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        session = Session.getSesstion(getApplicationContext());
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                signup();
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

            }
        });
    }

    //signup bottom clicked
    public void signup() {
        if (!validate()) {
            onSignupFailed();
            return;
        }
        final ProgressDialog progress = new ProgressDialog(this,
                R.style.SpinnerTheme);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("Loading ...");
        progress.show();
        user = new User();
        user.setFirstName(firstname.getText().toString());
        user.setLastName(lastname.getText().toString());
        user.setUsername(email.getText().toString());
        user.setPassword(password.getText().toString());
        user.setPhoto_uri(photo_uri.getText().toString());

        UserWebService userWebService = RetrofitClientInstance.getRetrofitInstance().create(UserWebService.class);
        Call<User> userCall = userWebService.postUser(user);
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "onResponse: " + response.code());
                if (response.isSuccessful()) {
                    if (response.code() == 201) {
                        session.setUser(response.body());
                        load();
                    } else {
                        Toast.makeText(getBaseContext(), "Failed to signup", Toast.LENGTH_LONG).show();

                    }

                }
                progress.dismiss();

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(getBaseContext(), "Failed to signup", Toast.LENGTH_LONG).show();

            }
        });

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        // onLoginSuccess();
                        //monta.garfa@gmail.com onLoginFailed();
                    }
                }, 3000);

    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        signup.setEnabled(true);
    }

    //validate signup
    public boolean validate() {
        boolean valid = true;

        String firstanameV = firstname.getText().toString();
        String lastnameV = lastname.getText().toString();
        String emailV = email.getText().toString();
        String passwordV = password.getText().toString();
        String repasswordV = repassword.getText().toString();
        String photo_uriV = photo_uri.getText().toString();

        if (firstanameV.isEmpty() || lastnameV.length() < 3) {
            firstname.setError("at least 3 chafragment_containerracters");
            valid = false;
        } else {
            firstname.setError(null);
        }

        if (lastnameV.isEmpty() || lastnameV.length() < 3) {
            lastname.setError("at least 3 characters");
            valid = false;
        } else {
            lastname.setError(null);
        }


        if (emailV.isEmpty()) {
            email.setError("enter a valid email address");
            valid = false;
        } else {
            email.setError(null);
        }

        if (passwordV.isEmpty() || passwordV.length() < 4 || passwordV.length() > 10) {
            password.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            password.setError(null);
        }

        if (repasswordV.isEmpty() || repasswordV.length() < 4 || repasswordV.length() > 10 || !(repasswordV.equals(passwordV))) {
            repassword.setError("Password Do not match");
            valid = false;
        } else {
            repassword.setError(null);
        }
        if (photo_uriV.isEmpty()) {
            photo_uri.setError("enter a valid uri");
            valid = false;
        } else {
            photo_uri.setError(null);
        }

        return valid;
    }

    public void load() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

    }
}

